**Introduction:**

AIFClust is the Galaxy project tool implementation of a series of scripts, which cluster the mass fragments from All-ion fragmentation LC-MS data onto the clusters created based on full-scan LC-MS data. The output results are mass spectra file in MGF format. See [my github repo.](https://github.com/jundahuang9123/msclust-aif)

The tool contains 6 functions which correspond to the 6 python scripts in the [tools/aifclust](https://git.wur.nl/huang089/aifclust/-/tree/master/tools/aifclust) folder. 

**Installation:**

To install the tool in your Galaxy instance, copy the [aifclust](https://git.wur.nl/huang089/aifclust/-/tree/master/tools/aifclust) folder into the tools directory of your Galaxy instance.

Add lines from [tool_conf_lines.xml](https://git.wur.nl/huang089/aifclust/-/blob/master/tool_conf_lines.xml) into the "toolbox" section of tool_conf.xml file in the config directory of your Galaxy instance. 

Add lines from [integrated_tool_panel_lines.xml](https://git.wur.nl/huang089/aifclust/-/blob/master/integrated_tool_panel_lines.xml) into the "toolbox" section of integrated_tool_panel.xml file in the config directory of your Galaxy instance.

**Dependencies:**

Packages required for the tool are:

![image.png](./image.png)

scipy version: 1.7.0

numpy version: 1.20.3

pyteomics version: 4.5.0

matchms version: 0.11.0
