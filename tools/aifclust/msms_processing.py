#!/usr/bin/env python
"""
Author: Junda Huang

Script reducing small peaks m/z under 90 in msms spectra

Command line input: python3 msms_processing.py msms.mgf cutoff less_msms.mgf
"""

# import statements
import os
from sys import argv

# functions

def parse_file(file, cut):
    """
    """
    new_file = []
    with open(file, 'r') as f:
        for line in f:
            #print(line)
            if len(line) > 1:
                if line[2] == "." or line[3] == ".":
                    if float(line[:4]) <= int(cut):
                        continue
                    else:
                        new_file.append(line)
                else:
                    new_file.append(line)
            else:
                new_file.append(line)
    return new_file

def write_newmgf(new_file, name):
    """
    """

    with open(name,'w') as f:
        for line in new_file:
            f.write(line)
    return

# main
if __name__ == '__main__':
    # parsing files directory from command line
    dir = argv[1]
    cutoff = argv[2]
    name = argv[3]
    new = parse_file(dir, cutoff)
    write_newmgf(new, name)
   