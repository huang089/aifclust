#!/usr/bin/env python
"""
Author: Junda Huang

Script using matchms scoring functions to get matching scores 
for imput spectra in mgf/msp again mgf/msp spetra references library
spectra

Command line input: python3 matchms_scoring.py <query>.mgf 
                    <referrence_library>.msp/mgf <output>.txt
"""

# import statements
from sys import argv
import numpy as np
from matchms.importing import load_from_mgf, load_from_msp
from matchms.filtering import default_filters, normalize_intensities
from matchms import calculate_scores, Spectrum, Scores
from matchms.similarity import CosineGreedy
from matchms.Spikes import Spikes
import logging
from matchms.plotting.spectrum_plots import plot_spectra_mirror
from matchms.plotting.spectrum_plots import plot_spectrum
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf

# functions

def matchms_score(q, q_spectra, ref_spectra, reference, ppmweight):
    """
    """
    for r in load_from_mgf(reference):
        tolerence = float(q.metadata['mass(ud)'])
        if 'pepmass' in r.metadata.keys():
            target = float(r.metadata['pepmass'][0])
            if abs(target - tolerence) <= ppmweight * tolerence * (10**(-6)):
                r = normalize_intensities(default_filters(r))
                ref_spectra.append(r)
    if ref_spectra != []:
        matches = calculate_scores(ref_spectra, q_spectra, CosineGreedy())
        return matches
    else:
        return None

def matchms_to_file(mgf_file, references_file, output, \
    output_pdf, ret, cos, ppmweight):
    """
    Find spectra matches from references library, calculate matches scores 
    and write into output file
    Params:
        mgf_file(string): file name of the query mgf file
        references_file(string): file name of the reference library file (msp)
        output(string): file name of the output text file
    Returns:
        output(string): file name of the output text file
    """
    output_dict = {}
    sum_dict = {}
    queries = load_from_mgf(mgf_file)
    with open (output, 'w') as out:
        for q in queries: 
            q_spectra = [normalize_intensities(default_filters(q))]
            ref_spectra = []
            matches = matchms_score(q, q_spectra, ref_spectra, \
                references_file, ppmweight)
            if matches != None:
                for match in matches:
                    (reference, query, match) = match
                    a = reference.metadata['pepmass'][0]
                    b = reference.metadata['rtinseconds']
                    c = query.metadata['mass(ud)']
                    d = query.metadata['retention']
                    e = match['score']
                    if reference is not query and match["matches"] >= 1 and \
                        abs((int(b)/60) - (int(d)/1000000))<= ret and e > 0:
                        out.write(f"Reference precursormz:{a}\n")
                        out.write(f"Reference rettime:{b}\n")
                        out.write(f"Query precursormz:{c}\n")
                        out.write(f"Query rettime:{d}\n")
                        out.write(f"Score:{e:.4f}\n")
                        out.write(f"Number of matching peaks:\
                        {match['matches']}\n")
                        out.write("----------------------------\n")
                        if query not in sum_dict.keys():
                            sum_dict[query] = e
                        elif sum_dict[query] < e:
                            sum_dict[query] = e
                        if query in output_dict.keys() and e >= cos:
                            output_dict[query].append((reference, e))
                        elif e >= cos:
                            output_dict[query] = []  
                            output_dict[query].append((reference, e))
    pdf = matplotlib.backends.backend_pdf.PdfPages(output_pdf)
    fig = plt.figure()
    for k in output_dict:
        for spec in output_dict[k]:
            pl = plot_spectra_mirror(spec[0], k)
            pl.plot()
            pl.set_title('Spectra Comparison; Cosine:{:.4f}\n\
            DDA:{}@{:.4f}min vs DIA:{}@{:.4f}min'.format\
            (spec[1], spec[0].metadata['pepmass'][0], \
            int(spec[0].metadata['rtinseconds'])/60, \
            k.metadata['mass(ud)'], int(k.metadata['retention'])/1000000))
            fig.axes.append(pl)
            pdf.savefig(fig)
    scores = []
    for q in sum_dict:
        scores.append(float(sum_dict[q]))
    bin_list = []
    fig2 = plt.figure()
    plt.hist(scores, color = 'green', edgecolor = 'black')
    plt.title('Distribution of metabolites scores against library')
    plt.xlabel('Cosine scores')
    plt.ylabel('Metabolites')
    pdf.savefig(fig2)
    pdf.close()
    return matches, sum_dict

def main(query, ref, output, output_pdf, ret, cos, ppmweight):
    '''
    The main function
    Params:
        mgf_file(string): file name of the query mgf file
        references_file(string): file name of the reference library file (msp)
        output(string): file name of the output text file
    Returns:
        output(string): file name of the output text file
    '''
    matches, s_dict = matchms_to_file(query, ref, output, \
    output_pdf, ret, cos, ppmweight)
    return matches

# main
if __name__ == '__main__':
    # parsing files from command line
    query = argv[1]
    references_library = argv[2]
    ret = float(argv[3])
    cos = float(argv[4])
    ppm = int(argv[5])
    output_file = argv[6]
    output_pdf = argv[7]

    # main
    logging.basicConfig()
    logger = logging.getLogger('matchms')
    logger.setLevel(logging.ERROR)
    scores = main(query, references_library, output_file, output_pdf, ret, \
    cos, ppm)